import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Character } from 'src/app/interfaces/charcter';
import { House } from 'src/app/interfaces/house';
import { GameofthronesService } from 'src/app/services/gameofthrones.service';

@Component({
  selector: 'app-house-details',
  templateUrl: './house-details.component.html',
  styleUrls: ['./house-details.component.css']
})
export class HouseDetailsComponent implements OnInit {
  house?: House
  currentLord?: Character
  heir?: Character
  overlord?: House
  founder?: Character
  cadetBranches: House[] = []
  swornMembers: Character[] = []
  

  constructor(private route: ActivatedRoute, private service: GameofthronesService ) { }

  /**
   * Beolvassa a paraméterként kapott azonosítóval az adott családot, és az urljei alapján beállítja az összes többi objektumát
   */

  ngOnInit(): void {
    this.route.paramMap.subscribe((param: any) => {
      if (param.params.key) {
        this.service.getHoues(param.params.key).subscribe((house: House) => {
          this.house = house;
          console.log(this.house.currentLord)
          if(this.house.currentLord !== undefined && this.house.currentLord != "") {
            this.service.getCharacterByUrl(this.house.currentLord).subscribe((character : Character) => {
              this.currentLord = character;
            });
          }
          if(this.house.heir !== undefined && this.house.heir != "") {
            this.service.getCharacterByUrl(this.house.heir).subscribe((character : Character) => {
              this.heir = character;
            });
          }
          if(this.house.overlord !== undefined && this.house.overlord != "") {
            this.service.getHouseByUrl(this.house.overlord).subscribe((house : House) => {
              this.overlord = house;
            });
          }
          if(this.house.founder !== undefined && this.house.founder != "") {
            this.service.getCharacterByUrl(this.house.founder).subscribe((character : Character) => {
              this.founder = character;
            });
          }
          if(this.house.cadetBranches !== undefined) {
            for (let i = 0; i < this.house.cadetBranches.length; i++) {
              this.service.getBookByUrl(this.house.cadetBranches[i]).subscribe((house : House) => {
                this.cadetBranches.push(house);
              });
            }
          }
          if(this.house.swornMembers !== undefined) {
            for (let i = 0; i < this.house.swornMembers.length; i++) {
              this.service.getBookByUrl(this.house.swornMembers[i]).subscribe((character : Character) => {
                this.swornMembers.push(character);
              });
            }
          }
        });
      }
    });
  }

   /**
   * Levágja az URL-t és visszadja azt a részét, amivel egy objektumot lehet azonosítani
   */
  
  key(str: string | undefined): string | undefined {
    if (str === undefined) {
      return undefined;
    }
    return str.split('/api')[1];
  }

}
