import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Character } from 'src/app/interfaces/charcter';
import { GameofthronesService } from 'src/app/services/gameofthrones.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css']
})
export class CharactersComponent implements OnInit {
  value = '';
  characters?: Character[]

  constructor(private service: GameofthronesService) { }

  /**
   * Beolvassa az összes karaktert, ami vicces, mert sajnos az api csak visszadja az első 10-et, akikről semmi információ nincs :D
   * de bent hagytam, mert ha a jövőben megcsinálnák az apit, akkor tök jól működne
   */

  ngOnInit(): void {
    let observable: Observable<Character[]> | undefined;
    observable = this.service.getCharacters();
    observable?.subscribe((characters: Character[]) => {
      this.characters = characters;
    });
  }

  /**
   * A paraméterként megadott név alapján megpróbálja megkeresni az adott karaktert
   */

  search(value: string): void {
    let observable: Observable<Character[]> | undefined;
    observable = this.service.searchCharacters(value);
    observable?.subscribe((characters: Character[]) => {
      this.characters = characters;
    });
  }

   /**
   * Levágja az URL-t és visszadja azt a részét, amivel egy objektumot lehet azonosítani
   */

  key(str: string | undefined): string | undefined {
    if (str === undefined) {
      return undefined;
    }
    return str.split('/api')[1];
  }

}
