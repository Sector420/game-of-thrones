import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { House } from 'src/app/interfaces/house';
import { GameofthronesService } from 'src/app/services/gameofthrones.service';

@Component({
  selector: 'app-houses',
  templateUrl: './houses.component.html',
  styleUrls: ['./houses.component.css']
})
export class HousesComponent implements OnInit {
  houses?: House[]

  constructor(private service: GameofthronesService) { }

  /**
   * Beolvassa az összes családot
   */

  ngOnInit(): void {
    let observable: Observable<House[]> | undefined;
    observable = this.service.getHouses();
    observable?.subscribe((houses: House[]) => {
      this.houses = houses;
    });
  }

  /**
   * Levágja az URL-t és visszadja azt a részét, amivel egy objektumot lehet azonosítani
   */

  key(str: string | undefined): string | undefined {
    if (str === undefined) {
      return undefined;
    }
    return str.split('/api')[1];
  }

}
