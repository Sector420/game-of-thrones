import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Book } from 'src/app/interfaces/book';
import { Character } from 'src/app/interfaces/charcter';
import { House } from 'src/app/interfaces/house';
import { GameofthronesService } from 'src/app/services/gameofthrones.service';

@Component({
  selector: 'app-character-details',
  templateUrl: './character-details.component.html',
  styleUrls: ['./character-details.component.css']
})
export class CharacterDetailsComponent implements OnInit {
  character?: Character
  father?: Character
  mother?: Character
  spouse?: Character
  houses: House[] = []
  books: Book[] = []
  povBooks: Book[] = []

  constructor(private route: ActivatedRoute, private service: GameofthronesService) { }

  /**
   * Beolvassa a paraméterként kapott azonosítóval az adott karaktert, és az urljei alapján beállítja az összes többi objektumát
   */

  ngOnInit(): void {
    this.route.paramMap.subscribe((param: any) => {
      if (param.params.key) {
        this.service.getCharacter(param.params.key).subscribe((character: Character) => {
          this.character = character;
          if(this.character.father !== undefined && this.character.father !="") {
            this.service.getCharacterByUrl(this.character.father).subscribe((character : Character) => {
              this.father = character;
            });
          }
          if(this.character.mother !== undefined && this.character.mother !="") {
            this.service.getCharacterByUrl(this.character.mother).subscribe((character : Character) => {
              this.mother = character;
            });
          }
          if(this.character.spouse !== undefined && this.character.spouse !="") {
            this.service.getCharacterByUrl(this.character.spouse).subscribe((character : Character) => {
              this.spouse = character;
            });
          }
          if(this.character.allegiances !== undefined) {
            for (let i = 0; i < this.character.allegiances.length; i++) {
              this.service.getHouseByUrl(this.character.allegiances[i]).subscribe((house : House) => {
                this.houses.push(house);
              });
            }
          }
          if(this.character.books !== undefined) {
            for (let i = 0; i < this.character.books.length; i++) {
              this.service.getBookByUrl(this.character.books[i]).subscribe((book : Book) => {
                this.books.push(book);
              });
            }
          }
          if(this.character.povBooks !== undefined) {
            for (let i = 0; i < this.character.povBooks.length; i++) {
              this.service.getBookByUrl(this.character.povBooks[i]).subscribe((book : Book) => {
                this.povBooks.push(book);
              });
            }
          }
        });
      }
    });
  }

   /**
   * Levágja az URL-t és visszadja azt a részét, amivel egy objektumot lehet azonosítani
   */

  key(str: string | undefined): string | undefined {
    if (str === undefined) {
      return undefined;
    }
    return str.split('/api')[1];
  }

}
