import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Book } from 'src/app/interfaces/book';
import { Character } from 'src/app/interfaces/charcter';
import { GameofthronesService } from 'src/app/services/gameofthrones.service';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {
  book?: Book
  characters: Character[] = []

  constructor(private route: ActivatedRoute, private service: GameofthronesService) { }

  /**
   * Beolvassa a paraméterként kapott azonosítóval az adott könyvet, és az urljei alapján beállítja az összes többi objektumát
   */

  ngOnInit(): void {
    this.route.paramMap.subscribe((param: any) => {
      if (param.params.key) {
        this.service.getBook(param.params.key).subscribe((book: Book) => {
          this.book = book;
          if(this.book.characters !== undefined) {
            for (let i = 0; i < this.book.characters.length; i++) {
              this.service.getCharacterByUrl(this.book.characters[i]).subscribe((character : Character) => {
                this.characters.push(character);
              });
            }
          }
        });
      }
    });
  }

  /**
   * Levágja a dátum lényegtelen adatait
   */

  date(str: string | undefined): string | undefined {
    if (str === undefined) {
      return undefined;
    }
    return str.split('T')[0];
  }

   /**
   * Levágja az URL-t és visszadja azt a részét, amivel egy objektumot lehet azonosítani
   */

  key(str: string | undefined): string | undefined {
    if (str === undefined) {
      return undefined;
    }
    return str.split('/api')[1];
  }

}
