import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Book } from 'src/app/interfaces/book';
import { GameofthronesService } from 'src/app/services/gameofthrones.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  books?: Book[]

  constructor(private service: GameofthronesService) { }

  /**
   * Beolvassa az összes könyvet
   */

  ngOnInit(): void {
    let observable: Observable<Book[]> | undefined;
    observable = this.service.getBooks();
    observable?.subscribe((books: Book[]) => {
      this.books = books;
    });
  }

   /**
   * Levágja az URL-t és visszadja azt a részét, amivel egy objektumot lehet azonosítani
   */

  key(str: string | undefined): string | undefined {
    if (str === undefined) {
      return undefined;
    }
    return str.split('/api')[1];
  }

}
