import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError  } from 'rxjs';
import { Book } from '../interfaces/book';
import { Character } from '../interfaces/charcter';
import { House } from '../interfaces/house';

/**
 * Ez az osztály felelős minden http kérés végrehajtásáért
 */

@Injectable({
  providedIn: 'root'
})
export class GameofthronesService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getBooks(): Observable<Book[]> {
    return this.httpClient.get<Book[]>(`https://www.anapioficeandfire.com/api/books`).pipe(
      catchError((error) => {
        alert(error.message);
        return throwError(() => error);
      })
    );
  }

  getBook(key: string): Observable<Book> {
    return this.httpClient.get<Book>(`https://www.anapioficeandfire.com/api/books/${key}`).pipe(
      catchError((error) => {
        alert(error.message);
        return throwError(() => error);
      })
    );
  }

  getBookByUrl(url: string): Observable<Character> {
    return this.httpClient.get<Book>(`${url}`).pipe(
      catchError((error) => {
        alert(error.message);
        return throwError(() => error);
      })
    );
  }

  getCharacters(): Observable<Character[]> {
    return this.httpClient.get<Character[]>(`https://www.anapioficeandfire.com/api/characters`).pipe(
      catchError((error) => {
        alert(error.message);
        return throwError(() => error);
      })
    );
  }

  getCharacter(key: string): Observable<Character> {
    return this.httpClient.get<Character>(`https://www.anapioficeandfire.com/api/characters/${key}`).pipe(
      catchError((error) => {
        alert(error.message);
        return throwError(() => error);
      })
    );
  }

  getCharacterByUrl(url: string): Observable<Character> {
    return this.httpClient.get<Character>(`${url}`).pipe(
      catchError((error) => {
        alert(error.message);
        return throwError(() => error);
      })
    );
  }

  searchCharacters(value: string): Observable<Character[]> {
    return this.httpClient.get<Character[]>(`https://www.anapioficeandfire.com/api/characters/?name=${value}`).pipe(
      catchError((error) => {
        alert(error.message);
        return throwError(() => error);
      })
    );
  }

  getHouses(): Observable<Book[]> {
    return this.httpClient.get<House[]>(`https://www.anapioficeandfire.com/api/houses`).pipe(
      catchError((error) => {
        alert(error.message);
        return throwError(() => error);
      })
    );
  }

  getHoues(key: string): Observable<Book> {
    return this.httpClient.get<House>(`https://www.anapioficeandfire.com/api/houses/${key}`).pipe(
      catchError((error) => {
        alert(error.message);
        return throwError(() => error);
      })
    );
  }

  getHouseByUrl(url: string): Observable<Character> {
    return this.httpClient.get<House>(`${url}`).pipe(
      catchError((error) => {
        alert(error.message);
        return throwError(() => error);
      })
    );
  }
}
